```
前端书单：（图书馆都有）
    HTML
        菜鸟教程 http://www.runoob.com/
            HTML、CSS、JS的基础都在这个上面学到的
    CSS
        CSS设计指南 http://item.jd.com/11232700.html
        CSS权威指南 http://item.jd.com/10100250.html

    JS
        JavaScript设计与开发新思维  http://item.jd.com/10352785124.html
        ppk谈JavaScript http://item.jd.com/10062730.html
        精通JavaScript    http://202.118.176.18:8080/opac/item.php?marc_no=0000286583（京东已经没了，图书馆有）
        JavaScript高级程序设计    http://item.jd.com/10951037.html
        JavaScript语言精粹  http://item.jd.com/11090963.html

进阶
    bootstrap   快速搭建网站

    HTML
        HTML5
    JS
        backbone    方便理解MVC
        Linux   阿里云服务器，大学生计划，10元/月
        Node    服务器端运行JS
            express 用这个搭建一个web服务器会加分
            koa
        ES6     JS新特性
        React   当下最流行的库
        Require.js 模块化入门
        gulp, webpack   构建工具
    CSS
        sass
        less

调试工具
    chrome
    Firefox

编辑器
    sublime text
    atom

知乎上的精彩问答（太多了，勤搜索）
    https://www.zhihu.com/question/39721183/answer/83536633
    https://www.zhihu.com/question/29933334/answer/46249405

tips:
    1. 善用搜索，使用Google，拒绝baidu
    2. 一定要边看边练习，在基础不牢固之前不要使用jQuery
    3. 善用微博、知乎、豆瓣等平台
        多关注“圈内人”
    4. 写博客，对于学习、找工作都是百利
    5. 善用github展示自己的能力

进阶
    怪异的JS [JavaScript Garden](http://bonsaiden.github.io/JavaScript-Garden/zh)
```